﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotation_test : MonoBehaviour
{
    public GameObject Prefab;
    public GameObject Side_left;
    public GameObject Side_right;
    public GameObject Back;
    float angle;
    public bool PrefabisAttached { get; set; };

       // Start is called before the first frame update
    void Awake()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        angle = GetAngle();

        if (angle >= 225.0f && angle <= 315.0f)
            AttachGameObject();
        else if (angle < 225.0f && angle > 135.0f)
            ChangeSpriteLeft();
        else if (angle <= 135.0f && angle >= 45.0f)
            ChangeSpriteRight();
        else if ((angle < 45.0f && angle > 0.0f) || (angle > 315.0f && angle < 360.0f))
            ChangeSpriteBack();
    }

    float GetAngle()
    {
        Vector3 direction = Camera.main.transform.position - this.transform.position;
        float angleTemp = Mathf.Atan2(direction.z, direction.x) * Mathf.Rad2Deg;
        angleTemp += 180.0f;
        angleTemp += 180.0f;
        return angleTemp;
    }

    public virtual void AttachGameObject()
    {
    GameObject.AttachGameObject()

            if (Prefab == null || PrefabisAttached || Prefab == GameObject)
            {
                return null;
            }
            GameObject go = Instantiate(Prefab);
        }
    }
    void ChangeSpriteLeft()
    {
        Instantiate(Side_left, new Vector3(0, 0, 0), Quaternion.identity);
    }
    void ChangeSpriteRight()
    {
        Instantiate(Side_right, new Vector3(0, 0, 0), Quaternion.identity);
    }
    void ChangeSpriteBack()
    {
        Instantiate(Back, new Vector3(0, 0, 0), Quaternion.identity);
    }

}
