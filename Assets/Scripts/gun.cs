﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gun : MonoBehaviour
{
    public float damage = 14f;
    public float range = 100f;

    public Camera GunCamera;

    Animator m_Animator;

    private void Start()
    {
        m_Animator = gameObject.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
     
        if (Input.GetButtonDown("Fire1"))
        {
            m_Animator.SetTrigger("fire");
        }
    }
}
